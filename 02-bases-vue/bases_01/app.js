//con vue, este crea un objeto de manera global llamado Vue
console.log(Vue);

// creación de una aplicacion con vue
const app = Vue.createApp({
  
  data(){
    return{
      quote: 'Soy Batman',
      author: 'Bruce Wayne',
      changed: false
    }
  },
  methods:{
    changeQuote( event ){
      this.changed = !this.changed;
      console.log('Hola Mundo', event);
      this.changed
        ? this.author = "Goku"
        : this.author = "Bruce Wine";
    },
    capitalize( ){
      this.quote = this.quote.toUpperCase();
    }

  }

});

app.mount('#myApp')

//-----------------------------------------------